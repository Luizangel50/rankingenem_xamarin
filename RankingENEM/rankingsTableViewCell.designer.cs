// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RankingENEM
{
	[Register ("rankingsTableViewCell")]
	public partial class rankingsTableViewCell
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		public UILabel alunosLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		public UILabel classEstadLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		public UILabel classMunLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		public UILabel classNacLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		public UILabel mediaEnemLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		public UILabel nomeEscolaLabel { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (alunosLabel != null) {
				alunosLabel.Dispose ();
				alunosLabel = null;
			}
			if (classEstadLabel != null) {
				classEstadLabel.Dispose ();
				classEstadLabel = null;
			}
			if (classMunLabel != null) {
				classMunLabel.Dispose ();
				classMunLabel = null;
			}
			if (classNacLabel != null) {
				classNacLabel.Dispose ();
				classNacLabel = null;
			}
			if (mediaEnemLabel != null) {
				mediaEnemLabel.Dispose ();
				mediaEnemLabel = null;
			}
			if (nomeEscolaLabel != null) {
				nomeEscolaLabel.Dispose ();
				nomeEscolaLabel = null;
			}
		}
	}
}
