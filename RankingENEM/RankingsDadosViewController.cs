using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;

using Mono.Data.Sqlite;

using Foundation;

using UIKit;

using CoreGraphics;

namespace RankingENEM
{
	public partial class RankingsDadosViewController : UIViewController
	{
		public RankingsDadosViewController (IntPtr handle) : base (handle)
		{
		}

		public string pracaSelected;
		public string estadoOuMunicipioSelected;
		public Municipios municipioSelected;

		public SqliteConnection connection = new SqliteConnection();
		public SqliteCommand command = new SqliteCommand ();
		private List<DadosBusca> dados = new List<DadosBusca> ();
		public DadosBusca dadoSelected;

		private string query;
		private string lastQuery;
		private string topLastQuery;
		private string conveniadasLastQuery;
		private string limited = "unlimited";

		public override void ViewDidLoad ()
		{
			rankingsScrollView.ScrollEnabled = true;

			base.ViewDidLoad ();

			NavigationItem.BackBarButtonItem  = new UIBarButtonItem ("Rankings", UIBarButtonItemStyle.Plain, null);
			NavigationItem.SetHidesBackButton (false, true);

			tipoPracaSelected();

			criterio_ordemButton.TouchUpInside += delegate {

				//var action = Xamarin.Forms.Page.DisplayActionSheet ("Imprimir ou Compartilhar", "Cancel", null, "Imprimir", "Email");

				UIAlertController actionSheetAlert = UIAlertController.Create("Critério de prioridade", "Selecione um critério", UIAlertControllerStyle.Alert);

				// Add Actions
				actionSheetAlert.AddAction(UIAlertAction.Create("Nota ENEM",UIAlertActionStyle.Default, (action) => {
					lastQuery = query + "ORDER BY CAST (MEDIA_ENEM AS DECIMAL) DESC";
					fillTableViewLabels(lastQuery);
				}));


				actionSheetAlert.AddAction(UIAlertAction.Create("Quantidade de Alunos",UIAlertActionStyle.Default, (action) => {					
					lastQuery = query + "ORDER BY CAST (TOTAL_ALUNOS AS INT) DESC ";
					fillTableViewLabels(lastQuery);
				}));

//				actionSheetAlert.AddAction(UIAlertAction.Create("Nota ENEM - CRESCENTE",UIAlertActionStyle.Default, (action) => {
//					lastQuery = query + "ORDER BY CAST (MEDIA_ENEM AS DECIMAL) ASC ";
//					fillTableViewLabels(lastQuery);
//				}));
//
//
//				actionSheetAlert.AddAction(UIAlertAction.Create("Quantidade de Alunos - CRESCENTE",UIAlertActionStyle.Default, (action) => {					
//					lastQuery = query + "ORDER BY CAST (TOTAL_ALUNOS AS INT) ASC ";
//					fillTableViewLabels(lastQuery);
//				}));

				conveniadasButton.SetTitle("Conveniadas: Todos", UIControlState.Normal);

				actionSheetAlert.AddAction(UIAlertAction.Create("Cancelar", UIAlertActionStyle.Cancel, (action) => {

				}));

				UIPopoverPresentationController presentationPopover = actionSheetAlert.PopoverPresentationController;
				if (presentationPopover!=null) {
					presentationPopover.SourceView = quantidadePosicoesLabel;
					presentationPopover.PermittedArrowDirections = UIPopoverArrowDirection.Any;
				}

				// Display the alert
				this.PresentViewController(actionSheetAlert,true,null);

			};

			conveniadasButton.TouchUpInside += delegate {

				UIAlertController actionSheetAlert = UIAlertController.Create("Conveniadas SAS", "Mostrar: ", UIAlertControllerStyle.Alert);

				// Add Actions
				actionSheetAlert.AddAction(UIAlertAction.Create("Apenas conveniadas",UIAlertActionStyle.Default, (action) => {
					List<DadosBusca> novosDados = new List<DadosBusca>();
					foreach (DadosBusca dado in dados) {
						if(dado.conveniada_sas.Contains("S")){
							novosDados.Add(dado);
						}
					}
					rankingsTableView.Hidden = true;
					rankingsTableView.Source = new RankingsTableSource (novosDados, this);
					rankingsTableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
					rankingsTableView.ReloadData ();
					rankingsTableView.Hidden = false;

					conveniadasButton.SetTitle("Conveniadas: SIM", UIControlState.Normal);
				}));


				actionSheetAlert.AddAction(UIAlertAction.Create("Apenas não-conveniadas",UIAlertActionStyle.Default, (action) => {					
					List<DadosBusca> novosDados = new List<DadosBusca>();
					foreach (DadosBusca dado in dados) {
						if(dado.conveniada_sas.Contains("N")){
							novosDados.Add(dado);
						}
					}
					rankingsTableView.Hidden = true;
					rankingsTableView.Source = new RankingsTableSource (novosDados, this);
					rankingsTableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
					rankingsTableView.ReloadData ();
					rankingsTableView.Hidden = false;

					conveniadasButton.SetTitle("Conveniadas: NÃO", UIControlState.Normal);
				}));
					
				actionSheetAlert.AddAction(UIAlertAction.Create("Todos",UIAlertActionStyle.Default, (action) => {					
					rankingsTableView.Hidden = true;
					rankingsTableView.Source = new RankingsTableSource (dados, this);
					rankingsTableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
					rankingsTableView.ReloadData ();
					rankingsTableView.Hidden = false;

					conveniadasButton.SetTitle("Conveniadas: Todos", UIControlState.Normal);
				}));

				actionSheetAlert.AddAction(UIAlertAction.Create("Cancelar", UIAlertActionStyle.Cancel, (action) => {

				}));

				UIPopoverPresentationController presentationPopover = actionSheetAlert.PopoverPresentationController;
				if (presentationPopover!=null) {
					presentationPopover.SourceView = quantidadePosicoesLabel;
					presentationPopover.PermittedArrowDirections = UIPopoverArrowDirection.Any;
				}

				// Display the alert
				this.PresentViewController(actionSheetAlert,true,null);

			};

			quantidadeTextField.EditingDidBegin += (sender, e) => {
				rankingsTableView.UserInteractionEnabled = false;
			};

			quantidadeTextField.EditingDidEnd += (sender, e) => {
				fillTableViewLabels(lastQuery + "LIMIT " + quantidadeTextField.Text);
				rankingsTableView.UserInteractionEnabled = true;
			};

			quantidadeTextField.ShouldReturn = delegate {
				quantidadeTextField.ResignFirstResponder ();
				return true;
			};
		}

		void tipoPracaSelected()
		{
			if (pracaSelected.Equals("Nacional")) {

				query =	"SELECT * FROM [geral] ";
				rankingsPracaLabel.Text += "Brasil";
			}
			else if(pracaSelected.Equals("Estadual")){
				
				query =	"SELECT * FROM [geral] WHERE " +
						"SIGLA_DA_UF LIKE '%" + 
						Listas.ufsiglas [Listas.ufnomes.IndexOf (estadoOuMunicipioSelected)] + "%' ";

				rankingsPracaLabel.Text += estadoOuMunicipioSelected;
			}
			else if(pracaSelected.Equals("Municipal")){
				
				query =	"SELECT * FROM [geral] WHERE " +
					"MUNICIPIO LIKE '%" +
					municipioSelected.municipio + 
					"%' AND SIGLA_DA_UF LIKE '%" +
					municipioSelected.unidfed + "%' ";

				rankingsPracaLabel.Text += municipioSelected.municipio + ", " + municipioSelected.unidfed;
			}
			lastQuery = query;
			fillTableViewLabels (lastQuery);
		}

		public void fillTableViewLabels (string queryFillTableViewLabels) {

			dados.Clear ();

			try {
				connection.ConnectionString = "Data Source=" + Listas.dbPath + ";Version=3;";
				command.Connection = connection;
				command.CommandText = queryFillTableViewLabels;
				connection.Open ();

				SqliteDataReader r = command.ExecuteReader ();

				while (r.Read ()) {
					dados.Add (new DadosBusca () {  
						codigo_inep = r.GetString (r.GetOrdinal ("CODIGO_INEP")),
						nome_escola = r.GetString (r.GetOrdinal ("NOME_ESCOLA")),
						conveniada_sas = r.GetString (r.GetOrdinal ("CONVENIADA_SAS")),
						codigo_crm_sas = r.GetInt32 (r.GetOrdinal ("CODIGO_CRM_SAS")),
						segmentos_sas = r.GetString (r.GetOrdinal ("SEGMENTOS_SAS")),
						alunos_sas = r.GetInt32 (r.GetOrdinal ("ALUNOS_SAS")),
						categoria_priv = r.GetString (r.GetOrdinal ("CATEGORIA_PRIV")),
						municipio = r.GetString (r.GetOrdinal ("MUNICIPIO")),
						sigla_da_uf = r.GetString (r.GetOrdinal ("SIGLA_DA_UF")),
						populacao_municipio = r.GetInt32 (r.GetOrdinal ("POPULACAO_MUNICIPIO")),
						regiao = r.GetString (r.GetOrdinal ("REGIAO")),
						cluster = r.GetString (r.GetOrdinal ("CLUSTER")),
						renda_per_capita_municipal = r.GetString (r.GetOrdinal ("RENDA_PER_CAPITA_MUNICIPAL")),
						nivel_socioeconomico = r.GetString (r.GetOrdinal ("NIVEL_SOCIOECONOMICO")),
						rua_numero = r.GetString (r.GetOrdinal ("RUA_NUMERO")),
						bairro = r.GetString (r.GetOrdinal ("BAIRRO")),
						complemento = r.GetString (r.GetOrdinal ("COMPLEMENTO")),
						cep = r.GetString (r.GetOrdinal ("CEP")),
						telefone = r.GetString (r.GetOrdinal ("TELEFONE")),
						media_enem = r.GetString (r.GetOrdinal ("MEDIA_ENEM")),
						nota_maior_540 = r.GetString (r.GetOrdinal ("NOTA_MAIOR_540")),
						colocacao_nacional = r.GetString (r.GetOrdinal ("COLOCACAO_NACIONAL")),
						classificacao_estadual = r.GetString (r.GetOrdinal ("CLASSIFICACAO_ESTADUAL")),
						classificacao_municipal = r.GetString (r.GetOrdinal ("CLASSIFICACAO_MUNICIPAL")),
						categoria_enem = r.GetString (r.GetOrdinal ("CATEGORIA_ENEM")),
						inf_ii_iii = r.GetString (r.GetOrdinal ("INF_II_III")),
						inf_iv_v = r.GetString (r.GetOrdinal ("INF_IV_V")),
						inf_unificado = r.GetString (r.GetOrdinal ("INF_UNIFICADO")),
						infantil = r.GetString (r.GetOrdinal ("INFANTIL")),
						ano_1o = r.GetString (r.GetOrdinal ("ANO_1o")),
						ano_2o = r.GetString (r.GetOrdinal ("ANO_2o")),
						ano_3o = r.GetString (r.GetOrdinal ("ANO_3o")),
						ano_4o = r.GetString (r.GetOrdinal ("ANO_4o")),
						ano_5o = r.GetString (r.GetOrdinal ("ANO_5o")),
						fun_i = r.GetString (r.GetOrdinal ("FUN_I")),
						ano_6o = r.GetString (r.GetOrdinal ("ANO_6o")),
						ano_7o = r.GetString (r.GetOrdinal ("ANO_7o")),
						ano_8o = r.GetString (r.GetOrdinal ("ANO_8o")),
						ano_9o = r.GetString (r.GetOrdinal ("ANO_9o")),
						fun_ii = r.GetString (r.GetOrdinal ("FUN_II")),
						serie_1a_em = r.GetString (r.GetOrdinal ("SERIE_1a_EM")),
						serie_2a_em = r.GetString (r.GetOrdinal ("SERIE_2a_EM")),
						pre_universitario = r.GetString (r.GetOrdinal ("PRE_UNIVERSITARIO")),
						em = r.GetString (r.GetOrdinal ("EM")),
						total_alunos = r.GetString (r.GetOrdinal ("TOTAL_ALUNOS")),
						categoria_alunos = r.GetString (r.GetOrdinal ("CATEGORIA_ALUNOS")),
						abc = r.GetString (r.GetOrdinal ("ABC")),
						a_partir_200_alunos = r.GetString (r.GetOrdinal ("A_PARTIR_200_ALUNOS")),
						segmentos = r.GetString (r.GetOrdinal ("SEGMENTOS"))
					});

				}
				r.Close ();

				connection.Close ();

				rankingsTableView.Hidden = true;
				//rankingsTableView.ScrollsToTop = true;
				//rankingsTableView.ReloadData ();
				rankingsTableView.Source = new RankingsTableSource (dados, this);
				rankingsTableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
				rankingsTableView.ReloadData ();
				rankingsTableView.Hidden = false;

			}
			catch {
				connection.Close ();
				var alert = new UIAlertView () { 
					Title = "InfoEscolas", 
					Message = "Insira apenas um número",
				};
				alert.AddButton ("OK");
				alert.Show ();
			}
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			quantidadeTextField.ResignFirstResponder ();
		}

		public override void PrepareForSegue (UIStoryboardSegue segue, NSObject sender)
		{
			base.PrepareForSegue (segue, sender);

			if (segue.Identifier.Equals("rankingDadosToInformacoesDaEscolaSegue")) {
				var informacoesEscolaViewController = segue.DestinationViewController as InformacoesEscolaViewController;

				if (informacoesEscolaViewController != null) {
					informacoesEscolaViewController.escolaSelected = dadoSelected;
				}
			}
		}
	}
}
