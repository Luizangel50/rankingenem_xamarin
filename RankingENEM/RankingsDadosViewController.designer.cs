// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RankingENEM
{
	[Register ("RankingsDadosViewController")]
	partial class RankingsDadosViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton conveniadasButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton criterio_ordemButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel quantidadePosicoesLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField quantidadeTextField { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIImageView rankingsImageView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel rankingsPracaLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		ScrollView rankingsScrollView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView rankingsTableView { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (conveniadasButton != null) {
				conveniadasButton.Dispose ();
				conveniadasButton = null;
			}
			if (criterio_ordemButton != null) {
				criterio_ordemButton.Dispose ();
				criterio_ordemButton = null;
			}
			if (quantidadePosicoesLabel != null) {
				quantidadePosicoesLabel.Dispose ();
				quantidadePosicoesLabel = null;
			}
			if (quantidadeTextField != null) {
				quantidadeTextField.Dispose ();
				quantidadeTextField = null;
			}
			if (rankingsImageView != null) {
				rankingsImageView.Dispose ();
				rankingsImageView = null;
			}
			if (rankingsPracaLabel != null) {
				rankingsPracaLabel.Dispose ();
				rankingsPracaLabel = null;
			}
			if (rankingsScrollView != null) {
				rankingsScrollView.Dispose ();
				rankingsScrollView = null;
			}
			if (rankingsTableView != null) {
				rankingsTableView.Dispose ();
				rankingsTableView = null;
			}
		}
	}
}
