﻿using System;
using System.Collections.Generic;

using Foundation;

using UIKit;

namespace RankingENEM
{
	//classe responsável pela inserção das células (UITableViewCells) na UITableView
	//da tela de resultados da busca
	public class EscolasTargetTableSource : UITableViewSource
	{
		private List<DadosBusca> _dados;
		private string CellIdentifier = "EscolasTargetTableCell";
		public DadosMercadoViewController _controller;

		public EscolasTargetTableSource (List<DadosBusca> dados, DadosMercadoViewController controller)
		{
			this._dados = dados;
			this._controller = controller;
		}

		public override nint RowsInSection(UITableView tableView, nint section) 
		{
			return _dados.Count;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (CellIdentifier);
			var item = _dados [indexPath.Row];

			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Default, CellIdentifier);
			}

			cell.TextLabel.Text = item.nome_escola;
			cell.TextLabel.Font = UIFont.SystemFontOfSize (12);

			return cell;
		}

		public override void RowSelected (UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			if (_dados.Count != 0) {
				_controller.dadoSelected = _dados [indexPath.Row];
				//RowDeselected (tableView, indexPath);
				_controller.PerformSegue ("dadosPracaToInformacoesDaEscolaSegue", _controller);
			}
		}
	}
}