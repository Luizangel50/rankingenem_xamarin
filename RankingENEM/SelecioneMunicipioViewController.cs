using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;

using Mono.Data.Sqlite;

using Foundation;

using MMLib.Extensions;

using UIKit;

namespace RankingENEM
{
	public partial class SelecioneMunicipioViewController : UIViewController
	{
		public SelecioneMunicipioViewController (IntPtr handle) : base (handle)
		{
		}

		UITableViewController municipioTableViewController;
		public string buttonIdentifier;

		public SqliteConnection connection = new SqliteConnection();
		public SqliteCommand command = new SqliteCommand ();

		private List<Municipios> municipios = new List<Municipios> ();
		public string municipioSearched;
		public Municipios municipioSelected;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			//botão de volta para a tela anterior
			NavigationItem.BackBarButtonItem  = new UIBarButtonItem ("Municípios", UIBarButtonItemStyle.Plain, null);
			NavigationItem.SetHidesBackButton (false, true);

			if (!(municipioSearched.IsNullOrWhiteSpace () || municipioSearched.IsNullOrWhiteSpace ())) {
				municipioSearched = municipioSearched.RemoveDiacritics ().Trim ();
			}

			string query =	"SELECT DISTINCT UPPER(MUNICIPIO) AS MUNICIPIO, SIGLA_DA_UF FROM [geral] WHERE " +
							"MUNICIPIO LIKE '%" + 
							municipioSearched + "%' GROUP BY SIGLA_DA_UF, MUNICIPIO ";

			connection.ConnectionString = "Data Source=" + Listas.dbPath + ";Version=3;";
			command.Connection = connection;
			command.CommandText = query;
			connection.Open ();

			SqliteDataReader r = command.ExecuteReader ();

			while (r.Read ()) {
				municipios.Add (new Municipios () {  
					municipio = r.GetString (r.GetOrdinal ("MUNICIPIO")),
					unidfed = r.GetString (r.GetOrdinal ("SIGLA_DA_UF"))
				});
			}

			r.Close ();

			connection.Close ();

			municipioTableView.Source = new MunicipioTableSource (municipios, this);
			municipioTableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;

			View.AddSubview (municipioTableView);

			if (municipios.Count == 0) {
				var alert = new UIAlertView () { 
					Title = "InfoEscolas", 
					Message = "Município não encontrado!",
				};
				alert.AddButton ("OK");
				alert.Show ();
			} 
		}


		public override void PrepareForSegue (UIStoryboardSegue segue, NSObject sender)
		{
			base.PrepareForSegue (segue, sender);

			if (segue.Identifier.Equals ("selecioneMunicipioToDadosPracaSegue")) {
				var dadosMercadoViewController = segue.DestinationViewController as DadosMercadoViewController;

				if (dadosMercadoViewController != null) {
					dadosMercadoViewController.pracaSelected = "Municipal";
					dadosMercadoViewController.municipioSelected = municipioSelected;
				}
			}

			else if (segue.Identifier.Equals ("selecioneMunicipioToRankingsDadosSegue")) {
				var rankingsDadosViewController = segue.DestinationViewController as RankingsDadosViewController;

				if (rankingsDadosViewController!= null) {
					rankingsDadosViewController.pracaSelected = "Municipal";
					rankingsDadosViewController.municipioSelected = municipioSelected;
				}
			}
		}
	}
}
