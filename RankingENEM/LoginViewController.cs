using System;
using System.Net;
using System.CodeDom.Compiler;
using System.IO;

//Biblioteca para o acesso dos Web Services
using RestSharp;

using Foundation;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using UIKit;

using CoreGraphics;

namespace RankingENEM
{
	//classe responsável pela tela de login
	partial class LoginViewController : UIViewController
	{
		//url da api de acesso
		static string url = @"http://servico.portalsas.com.br:8080/api/GlobalAuth/genericlogin";
		bool autenticado = false;

		//Rest Client
		RestClient client = new RestClient (url);
		private string loginFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "RankingLoginFile.txt");
		
		//implementar depois
		private static NSDate dataApp;
		private static NSComparisonResult dataComparacao;

		private KeyboardNotifications loginKeyboard;

		public LoginViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad() {			
			base.ViewDidLoad ();

			VerifyAccount ();
		}

		public void VerifyAccount() {
//			loginKeyboard = new KeyboardNotifications (View, null);
//			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.DidShowNotification, loginKeyboard.KeyBoardUpNotification);
//			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, loginKeyboard.KeyBoardDownNotification);	
//			NavigationItem.SetHidesBackButton (true, true);
			if (File.Exists (loginFile)) {				
				//if (dataApp != null && dataApp.SecondsSinceReferenceDate <= 1728000)
				NavigationItem.SetHidesBackButton (false, true);
				NavigationController.NavigationBar.PopNavigationItem (false);
				NavigationController.PopViewController (false);
				this.PerformSegue ("loginPageToBuscarPageSegue", this);
			} 
			else {
				loginKeyboard = new KeyboardNotifications (View, null);
				NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.DidShowNotification, loginKeyboard.KeyBoardUpNotification);
				NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, loginKeyboard.KeyBoardDownNotification);
				NavigationItem.SetHidesBackButton (false, true);
			}
		}

		public override bool ShouldPerformSegue (string segueIdentifier, NSObject sender)
		{
			var request = new RestRequest (Method.POST);

			request.AddParameter ("login", usuarioTextField.Text);
			request.AddParameter ("password", senhaTextField.Text);
//			request.AddParameter ("login", "geisealves@aridesa.com.br");
//			request.AddParameter ("password", "sas@2015");
			request.AddParameter ("tipoUsuario", 5);


			try {
					
				IRestResponse response = client.Execute (request);
				var content = response.Content;
				Console.WriteLine (content);

				var contentJson = JObject.Parse (content);

				foreach (var pair in contentJson) {

					if (pair.Key.Equals ("status")) {
						if (pair.Value.ToString ().Equals ("False")) {
							autenticado = false;
						} else if (pair.Value.ToString ().Equals ("True")) {
							dataApp = NSDate.Now;
							File.WriteAllText (loginFile, usuarioTextField + "\n" + senhaTextField);
							autenticado = true;
						}
					}
				}

				if (!autenticado) {
					var alert = new UIAlertView () { 
						Title = "InfoEscolas", 
						Message = "Usuário/senha inválido(s)!",
					};
					alert.AddButton ("OK");
					alert.Show ();
				}
			}

			catch {
				var alert = new UIAlertView () { 
					Title = "InfoEscolas", 
					Message = "Não foi possível efetuar o login.\nVerifique sua conexão com a internet.",
				};
				alert.AddButton ("OK");
				alert.Show ();
			}

			return autenticado;
		}

		public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender) {
			base.PrepareForSegue (segue, sender);

		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			usuarioTextField.ResignFirstResponder ();
			senhaTextField.ResignFirstResponder ();
		}
	}
}