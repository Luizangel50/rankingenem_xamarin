using System;
using System.Globalization;
using System.CodeDom.Compiler;
using System.Collections.Generic;

using Foundation;

using UIKit;

using Mono.Data.Sqlite;

using CoreGraphics;

namespace RankingENEM
{
	public partial class DadosMercadoViewController : UIViewController
	{
		public string pracaSelected;
		public string estadoOuMunicipioSelected;
		public DadosBusca dadoSelected;

		public SqliteConnection connection = new SqliteConnection();
		public SqliteCommand command = new SqliteCommand ();
		private List<DadosBusca> dados = new List<DadosBusca> ();
		private List<DadosBusca> escolasTarget = new List<DadosBusca> ();
		private List<Municipios> municipios = new List<Municipios> ();
		private string query;

		LoadingOverlay loadingOverlay;

		public Municipios municipioSelected;

		public DadosMercadoViewController (IntPtr handle) : base (handle)
		{
		}
			
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			//botão de volta para a tela anterior
			NavigationItem.BackBarButtonItem  = new UIBarButtonItem ("Dados Mercado", UIBarButtonItemStyle.Plain, null);
			NavigationItem.SetHidesBackButton (false, true);

			tipoPracaSelected ();

			rankingsButton.TouchUpInside += (sender, e) => {

				this.PerformSegue("dadosPracaToRankingsDadosSegue", this);
			};
		}

		void tipoPracaSelected()
		{
			dados.Clear();
			municipios.Clear ();


			if (pracaSelected.Equals("Nacional")) {
				
				dadosPracaLabel.Text = dadosPracaLabel.Text + "Brasil";

				query =	"SELECT * FROM [geral]";
			}
			else if(pracaSelected.Equals("Estadual")){
				
				dadosPracaLabel.Text = dadosPracaLabel.Text + estadoOuMunicipioSelected;

				query =	"SELECT * FROM [geral] WHERE " +
		               	"SIGLA_DA_UF LIKE '%" + 
						Listas.ufsiglas [Listas.ufnomes.IndexOf (estadoOuMunicipioSelected)] + "%' ";
			}
			else if(pracaSelected.Equals("Municipal")){
				dadosPracaLabel.Text = dadosPracaLabel.Text + municipioSelected.municipio + ", " + municipioSelected.unidfed;

				query =	"SELECT * FROM [geral] WHERE " +
						"MUNICIPIO LIKE '%" +
						municipioSelected.municipio + 
						"%' AND SIGLA_DA_UF LIKE '%" +
						municipioSelected.unidfed + "%'";
			}

			fillLabels ();
		}

		public void fillLabels () {

			CultureInfo brasil = new CultureInfo ("pt-BR");

			connection.ConnectionString = "Data Source=" + Listas.dbPath + ";Version=3;";
			command.Connection = connection;
			command.CommandText = query;
			connection.Open ();

			SqliteDataReader r = command.ExecuteReader ();

			while (r.Read ()) {
				dados.Add (new DadosBusca () {  
					codigo_inep = r.GetString (r.GetOrdinal ("CODIGO_INEP")),
					nome_escola = r.GetString (r.GetOrdinal ("NOME_ESCOLA")),
					conveniada_sas = r.GetString (r.GetOrdinal ("CONVENIADA_SAS")),
					codigo_crm_sas = r.GetInt32 (r.GetOrdinal ("CODIGO_CRM_SAS")),
					segmentos_sas = r.GetString (r.GetOrdinal ("SEGMENTOS_SAS")),
					alunos_sas = r.GetInt32 (r.GetOrdinal ("ALUNOS_SAS")),
					categoria_priv = r.GetString (r.GetOrdinal ("CATEGORIA_PRIV")),
					municipio = r.GetString (r.GetOrdinal ("MUNICIPIO")),
					sigla_da_uf = r.GetString (r.GetOrdinal ("SIGLA_DA_UF")),
					populacao_municipio = r.GetInt32 (r.GetOrdinal ("POPULACAO_MUNICIPIO")),
					regiao = r.GetString (r.GetOrdinal ("REGIAO")),
					cluster = r.GetString (r.GetOrdinal ("CLUSTER")),
					renda_per_capita_municipal = r.GetString (r.GetOrdinal ("RENDA_PER_CAPITA_MUNICIPAL")),
					nivel_socioeconomico = r.GetString (r.GetOrdinal ("NIVEL_SOCIOECONOMICO")),
					rua_numero = r.GetString (r.GetOrdinal ("RUA_NUMERO")),
					bairro = r.GetString (r.GetOrdinal ("BAIRRO")),
					complemento = r.GetString (r.GetOrdinal ("COMPLEMENTO")),
					cep = r.GetString (r.GetOrdinal ("CEP")),
					telefone = r.GetString (r.GetOrdinal ("TELEFONE")),
					media_enem = r.GetString (r.GetOrdinal ("MEDIA_ENEM")),
					nota_maior_540 = r.GetString (r.GetOrdinal ("NOTA_MAIOR_540")),
					colocacao_nacional = r.GetString (r.GetOrdinal ("COLOCACAO_NACIONAL")),
					classificacao_estadual = r.GetString (r.GetOrdinal ("CLASSIFICACAO_ESTADUAL")),
					classificacao_municipal = r.GetString (r.GetOrdinal ("CLASSIFICACAO_MUNICIPAL")),
					categoria_enem = r.GetString (r.GetOrdinal ("CATEGORIA_ENEM")),
					inf_ii_iii = r.GetString (r.GetOrdinal ("INF_II_III")),
					inf_iv_v = r.GetString (r.GetOrdinal ("INF_IV_V")),
					inf_unificado = r.GetString (r.GetOrdinal ("INF_UNIFICADO")),
					infantil = r.GetString (r.GetOrdinal ("INFANTIL")),
					ano_1o = r.GetString (r.GetOrdinal ("ANO_1o")),
					ano_2o = r.GetString (r.GetOrdinal ("ANO_2o")),
					ano_3o = r.GetString (r.GetOrdinal ("ANO_3o")),
					ano_4o = r.GetString (r.GetOrdinal ("ANO_4o")),
					ano_5o = r.GetString (r.GetOrdinal ("ANO_5o")),
					fun_i = r.GetString (r.GetOrdinal ("FUN_I")),
					ano_6o = r.GetString (r.GetOrdinal ("ANO_6o")),
					ano_7o = r.GetString (r.GetOrdinal ("ANO_7o")),
					ano_8o = r.GetString (r.GetOrdinal ("ANO_8o")),
					ano_9o = r.GetString (r.GetOrdinal ("ANO_9o")),
					fun_ii = r.GetString (r.GetOrdinal ("FUN_II")),
					serie_1a_em = r.GetString (r.GetOrdinal ("SERIE_1a_EM")),
					serie_2a_em = r.GetString (r.GetOrdinal ("SERIE_2a_EM")),
					pre_universitario = r.GetString (r.GetOrdinal ("PRE_UNIVERSITARIO")),
					em = r.GetString (r.GetOrdinal ("EM")),
					total_alunos = r.GetString (r.GetOrdinal ("TOTAL_ALUNOS")),
					categoria_alunos = r.GetString (r.GetOrdinal ("CATEGORIA_ALUNOS")),
					abc = r.GetString (r.GetOrdinal ("ABC")),
					a_partir_200_alunos = r.GetString (r.GetOrdinal ("A_PARTIR_200_ALUNOS")),
					segmentos = r.GetString (r.GetOrdinal ("SEGMENTOS"))
				});

			}
			r.Close ();

			connection.Close ();


			int sumAuxTotalAlunos = 0;
			int sumAuxPopulacao = 0;
			int sumAuxAlunosSAS = 0;
			int sumAuxConveniadaSAS = 0;
			double auxMediaENEMPraca = 0;
			double mediaENEMPraca = 0;
			int escolasENEM = 0;
			double mediaAlunosPraca = 0;

			foreach (var dado in dados) {
				//Campo TotalAlunos
				if (!dado.total_alunos.Contains ("N/A")) {
					sumAuxTotalAlunos += Convert.ToInt32 (dado.total_alunos); 
				}

				//Campo Populaçao
				sumAuxPopulacao += dado.populacao_municipio;

				//Campo Alunos SAS
				sumAuxAlunosSAS += dado.alunos_sas;

				//Campo Escolas SAS
				if (dado.conveniada_sas.Contains("SIM")) {
					sumAuxConveniadaSAS += 1;
				}

				//Escolas Target
				if (!dado.abc.Contains ("N/A") && dado.abc.Contains("sim")) {
					escolasTarget.Add (dado);
				}

				//Media Enem Praca
				if (Double.TryParse (dado.media_enem, out auxMediaENEMPraca)) {
					auxMediaENEMPraca = double.Parse (dado.media_enem, System.Globalization.CultureInfo.InvariantCulture);

					if (auxMediaENEMPraca != 0) {
						mediaENEMPraca += auxMediaENEMPraca;
						escolasENEM++;
					}
				}
			}

			//Preenchendo labels
			totalAlunosLabel.Text += sumAuxTotalAlunos.ToString ("N0", brasil.NumberFormat);
			alunosSASLabel.Text += sumAuxAlunosSAS.ToString("N0", brasil.NumberFormat);
			try {
				shareAlunosLabel.Text += 
					decimal.Round((decimal) 100*sumAuxAlunosSAS / sumAuxTotalAlunos,2, MidpointRounding.AwayFromZero).ToString("0.##").Replace('.', ',') + 
					"%";
			}
			catch {
				shareAlunosLabel.Text += "0";
			}

			escolasLabel.Text += dados.Count.ToString("N0", brasil.NumberFormat);
			escolasSASLabel.Text += sumAuxConveniadaSAS.ToString("N0", brasil.NumberFormat);
			try {
				shareEscolasLabel.Text += 
					decimal.Round((decimal) 100*sumAuxConveniadaSAS/dados.Count,2, MidpointRounding.AwayFromZero).ToString("0.##").Replace('.', ',') + 
					"%";
			}
			catch {
				shareEscolasLabel.Text += "0";
			}

			mediaENEMPraca = mediaENEMPraca / escolasENEM;
			if(!Double.IsNaN(mediaENEMPraca))
				mediaENEMPracaLabel.Text += mediaENEMPraca.ToString ("N2", brasil.NumberFormat);
			else
				mediaENEMPracaLabel.Text += "0,00";

			mediaAlunosPraca = sumAuxTotalAlunos / dados.Count;
			mediaAlunosEscolaLabel.Text += mediaAlunosPraca.ToString ("N0", brasil.NumberFormat);

			if (pracaSelected.Equals ("Municipal") && dados.Count != 0) {
				populacaoLabel.Text += dados [0].populacao_municipio.ToString("N0", brasil.NumberFormat);

				double renda_per_capita;
				if (Double.TryParse(dados [0].renda_per_capita_municipal, out renda_per_capita))
					rendaPerCapitaLabel.Text += double.Parse(dados [0].renda_per_capita_municipal, System.Globalization.CultureInfo.InvariantCulture).ToString("C", brasil.NumberFormat);							
				else
					rendaPerCapitaLabel.Text += dados [0].renda_per_capita_municipal;

				escolasTargetLabel.Text += escolasTarget.Count.ToString("N0", brasil.NumberFormat);;
				if (escolasTarget.Count > 0) {
					escolasTargetTableView.Source = new EscolasTargetTableSource (escolasTarget, this);
					escolasTargetTableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
				}
				else {
					escolasTargetLabel.Hidden = true;
					escolasTargetTableView.Hidden = true;
					rankingsButtonToTableViewConstraint.Active = false;
					rankingsButtonToLabelsConstraint.Constant = 10;
					rendaPerCapitaToTableViewConstraint.Active = false;
					rendaPerCapitaToMediaAlunosLabelsConstraint.Constant = 10;
					rankingsButton.UpdateConstraints ();
					rendaPerCapitaLabel.UpdateConstraints ();
				}


			}
			else if (!pracaSelected.Equals("Municipal")) {
				escolasTargetLabel.Text += escolasTarget.Count.ToString("N0", brasil.NumberFormat);
				if (escolasTarget.Count > 0) {
					escolasTargetTableView.Source = new EscolasTargetTableSource (escolasTarget, this);
					escolasTargetTableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
				}
				else {
					escolasTargetLabel.Hidden = true;
					escolasTargetTableView.Hidden = true;
				}

				populacaoLabel.Hidden = true;
				rendaPerCapitaLabel.Hidden = true;

				rankingsButtonToLabelsConstraint.Active = false;
				rankingsButtonToTableViewConstraint.Constant = 10;
				rankingsButton.UpdateConstraints ();
			}

		}

		public override void PrepareForSegue (UIStoryboardSegue segue, NSObject sender)
		{
			base.PrepareForSegue (segue, sender);

			if (segue.Identifier.Equals ("dadosPracaToRankingsDadosSegue")) {
				var rankingsDadosViewController = segue.DestinationViewController as RankingsDadosViewController;

				if (rankingsDadosViewController!= null) {
					rankingsDadosViewController.pracaSelected = pracaSelected;
					rankingsDadosViewController.estadoOuMunicipioSelected = estadoOuMunicipioSelected;
					rankingsDadosViewController.municipioSelected = municipioSelected;
				}
			}
			else if (segue.Identifier.Equals("dadosPracaToInformacoesDaEscolaSegue")) {
				var informacoesEscolaViewController = segue.DestinationViewController as InformacoesEscolaViewController;

				if (informacoesEscolaViewController != null) {
					informacoesEscolaViewController.escolaSelected = dadoSelected;
				}
			}
		}

	}
}
