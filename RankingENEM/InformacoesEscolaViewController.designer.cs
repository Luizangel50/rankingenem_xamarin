// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RankingENEM
{
	[Register ("InformacoesEscolaViewController")]
	partial class InformacoesEscolaViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel codigoCRMLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel codigoINEPLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel conveniadaSASLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel enderecoLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel nomeEscolaLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel notaENEMLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel segmentosLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel segmentosSASLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel telefoneLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel totalAlunosLabel { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (codigoCRMLabel != null) {
				codigoCRMLabel.Dispose ();
				codigoCRMLabel = null;
			}
			if (codigoINEPLabel != null) {
				codigoINEPLabel.Dispose ();
				codigoINEPLabel = null;
			}
			if (conveniadaSASLabel != null) {
				conveniadaSASLabel.Dispose ();
				conveniadaSASLabel = null;
			}
			if (enderecoLabel != null) {
				enderecoLabel.Dispose ();
				enderecoLabel = null;
			}
			if (nomeEscolaLabel != null) {
				nomeEscolaLabel.Dispose ();
				nomeEscolaLabel = null;
			}
			if (notaENEMLabel != null) {
				notaENEMLabel.Dispose ();
				notaENEMLabel = null;
			}
			if (segmentosLabel != null) {
				segmentosLabel.Dispose ();
				segmentosLabel = null;
			}
			if (segmentosSASLabel != null) {
				segmentosSASLabel.Dispose ();
				segmentosSASLabel = null;
			}
			if (telefoneLabel != null) {
				telefoneLabel.Dispose ();
				telefoneLabel = null;
			}
			if (totalAlunosLabel != null) {
				totalAlunosLabel.Dispose ();
				totalAlunosLabel = null;
			}
		}
	}
}
