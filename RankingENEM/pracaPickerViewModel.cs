﻿using System;
using System.Collections.Generic;
using CoreGraphics;

using UIKit;

namespace RankingENEM
{
	public class pracaPickerViewModel : UIPickerViewModel
	{
		public pracaPickerViewModel() {
			
		}
		public event EventHandler<PickerChangedEventArgs> PickerChanged;

		public override void Selected (UIPickerView picker, nint row, nint component)
		{
			if (this.PickerChanged != null)
			{
				this.PickerChanged(this, new PickerChangedEventArgs{SelectedValue = Listas.pracas[(int)row]});
			}
		}

		public override nint GetComponentCount (UIPickerView picker)
		{
			return 1;
		}

		public override nint GetRowsInComponent (UIPickerView picker, nint component)
		{
			return Listas.pracas.Count;
		}

		public override string GetTitle (UIPickerView picker, nint row, nint component)
		{
			return Listas.pracas[(int)row].ToString();
		}

		public override nfloat GetRowHeight (UIPickerView picker, nint component)
		{
			return 30f;
		}

//		public override UIView GetView (UIPickerView picker, nint row, nint component, UIView view)
//		{
//			//Lazy initialize
//			if (view == null) {
//				CGSize rowSize = picker.RowSizeForComponent (component);
//				view = new UIView (new CGRect (new CGPoint (0, 0), rowSize));
//			}
//			//Modify state to reflect data
//			view.BackgroundColor = UIColor.White;
//			return view;
//		}

	}

	public class PickerChangedEventArgs : EventArgs
	{
		public string SelectedValue {get; set;}
	}
}

