﻿using System;
using System.Globalization;
using System.Collections.Generic;

using Foundation;

using UIKit;

namespace RankingENEM
{
	//classe responsável pela inserção das células (UITableViewCells) na UITableView
	//da tela de resultados da busca
	public class RankingsTableSource : UITableViewSource
	{
		private List<DadosBusca> _dados;
		private string CellIdentifier = "RankingsTableCell";
		public RankingsDadosViewController _controller;

		public RankingsTableSource (List<DadosBusca> dados, RankingsDadosViewController controller)
		{
			this._dados = dados;
			this._controller = controller;
		}

		public override nint RowsInSection(UITableView tableView, nint section) 
		{
			return _dados.Count;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			rankingsTableViewCell cell  = tableView.DequeueReusableCell (CellIdentifier) as rankingsTableViewCell;
			var item = _dados [indexPath.Row];

			if (cell == null) {
				cell = new rankingsTableViewCell (UITableViewCellStyle.Default, CellIdentifier);
			}

			cell.nomeEscolaLabel.Text = item.nome_escola;

			CultureInfo brasil = new CultureInfo ("pt-BR");

			double media_enem;
			int teste;
			if (Double.TryParse(item.media_enem , out media_enem))
				cell.mediaEnemLabel.Text = double.Parse(item.media_enem, System.Globalization.CultureInfo.InvariantCulture).ToString("N2", brasil.NumberFormat);
			else
				cell.mediaEnemLabel.Text = item.media_enem;
			
			cell.classMunLabel.Text = item.classificacao_municipal;
			if (int.TryParse (item.classificacao_municipal, out teste))
				cell.classMunLabel.Text += "°";
			
			cell.classEstadLabel.Text = item.classificacao_estadual;
			if (int.TryParse (item.classificacao_estadual, out teste))
				cell.classEstadLabel.Text += "°";


			cell.classNacLabel.Text = item.colocacao_nacional;
			if (int.TryParse (item.colocacao_nacional, out teste))
				cell.classNacLabel.Text += "°";			


			int total_alunos;
			if (int.TryParse (item.total_alunos, out total_alunos))
				cell.alunosLabel.Text = total_alunos.ToString ("N0", brasil.NumberFormat);
			else
				cell.alunosLabel.Text = item.total_alunos;

			return cell;
		}

		public override void RowSelected (UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			if (_dados.Count != 0) {
				_controller.dadoSelected = _dados [indexPath.Row];
				_controller.PerformSegue ("rankingDadosToInformacoesDaEscolaSegue", _controller);
			}
		}
	}
}