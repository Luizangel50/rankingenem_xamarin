﻿using System;
using System.Windows;
using System.Threading;
using System.Collections.Generic;
using System.IO;

using SQLite;

//Classes utilizadas para armazenamento no banco dos dados do arquivo .csv
//e armazenamento dos dados das buscas
namespace RankingENEM
{
	[Table("geral")]
	public class BaseGeral {
		[MaxLength(255), NotNull]
		public string CODIGO_INEP {get;set;}

		[MaxLength(255), NotNull]
		public string NOME_ESCOLA {get;set;}

		[MaxLength(3),NotNull]
		public string CONVENIADA_SAS {get;set;}

		[NotNull]
		public int CODIGO_CRM_SAS {get;set;}

		[MaxLength(255), NotNull]
		public string SEGMENTOS_SAS {get;set;}

		[NotNull]
		public int ALUNOS_SAS {get;set;}

		[MaxLength(255), NotNull]
		public string CATEGORIA_PRIV {get;set;}

		[MaxLength(255), NotNull]
		public string MUNICIPIO {get;set;}

		[MaxLength(2), NotNull]
		public string SIGLA_DA_UF {get;set;}

		[NotNull]
		public int POPULACAO_MUNICIPIO {get;set;}

		[MaxLength(2),NotNull]
		public string REGIAO {get;set;}

		[MaxLength(8),NotNull]
		public string CLUSTER {get;set;}

		[MaxLength(255), NotNull]
		public string RENDA_PER_CAPITA_MUNICIPAL {get;set;}

		[MaxLength(255), NotNull]
		public string NIVEL_SOCIOECONOMICO {get;set;}

		[MaxLength(255), NotNull]
		public string RUA_NUMERO {get;set;}

		[MaxLength(255), NotNull]
		public string BAIRRO {get;set;}

		[MaxLength(255), NotNull]
		public string COMPLEMENTO {get;set;}

		[MaxLength(255), NotNull]
		public string CEP {get;set;}

		[MaxLength(255), NotNull]
		public string TELEFONE {get;set;}

		[MaxLength(255), NotNull]
		public string MEDIA_ENEM {get;set;}

		[MaxLength(3), NotNull]
		public string NOTA_MAIOR_540 {get;set;}

		[MaxLength(255), NotNull]
		public string COLOCACAO_NACIONAL {get;set;}

		[MaxLength(255), NotNull]
		public string CLASSIFICACAO_ESTADUAL {get;set;}

		[MaxLength(255), NotNull]
		public string CLASSIFICACAO_MUNICIPAL {get;set;}

		[MaxLength(255), NotNull]
		public string CATEGORIA_ENEM {get;set;}

		[MaxLength(255), NotNull]
		public string INF_II_III {get;set;}

		[MaxLength(255), NotNull]
		public string INF_IV_V {get;set;}

		[MaxLength(255), NotNull]
		public string INF_UNIFICADO {get;set;}

		[MaxLength(255), NotNull]
		public string INFANTIL {get;set;}

		[MaxLength(255), NotNull]
		public string ANO_1o {get;set;}

		[MaxLength(255), NotNull]
		public string ANO_2o {get;set;}

		[MaxLength(255), NotNull]
		public string ANO_3o {get;set;}

		[MaxLength(255), NotNull]
		public string ANO_4o {get;set;}

		[MaxLength(255), NotNull]
		public string ANO_5o {get;set;}

		[MaxLength(255), NotNull]
		public string FUN_I {get;set;}

		[MaxLength(255), NotNull]
		public string ANO_6o {get;set;}

		[MaxLength(255), NotNull]
		public string ANO_7o {get;set;}

		[MaxLength(255), NotNull]
		public string ANO_8o {get;set;}

		[MaxLength(255), NotNull]
		public string ANO_9o {get;set;}

		[MaxLength(255), NotNull]
		public string FUN_II {get;set;}

		[MaxLength(255), NotNull]
		public string SERIE_1a_EM {get;set;}

		[MaxLength(255), NotNull]
		public string SERIE_2a_EM {get;set;}

		[MaxLength(255), NotNull]
		public string PRE_UNIVERSITARIO {get;set;}

		[MaxLength(255), NotNull]
		public string EM {get;set;}

		[MaxLength(255), NotNull]
		public string TOTAL_ALUNOS {get;set;}

		[MaxLength(255), NotNull]
		public string CATEGORIA_ALUNOS {get;set;}

		[MaxLength(255), NotNull]
		public string ABC {get;set;}

		[MaxLength(255), NotNull]
		public string A_PARTIR_200_ALUNOS {get;set;}

		[MaxLength(255), NotNull]
		public string SEGMENTOS {get;set;}
	}

	[Table("ano")]
	public class Ano {
		[PrimaryKey, AutoIncrement, Column("_id"), NotNull]
		public int ANO { get; set;}

		[NotNull, Unique]
		public int ANO_NUMERO { get; set; }

	}

	[Table("uf")]
	public class UF {
		[PrimaryKey, AutoIncrement, Column("_id"), NotNull]
		public int UNIDF { get; set; }

		[MaxLength(2),NotNull,Unique]
		public string UNIDF_SIGLA { get; set;}

		[NotNull,Unique]
		public string UNIDF_NOME { get; set; }
	}

	public class DadosBusca
	{
		public string codigo_inep {get;set;}

		public string nome_escola {get;set;}

		public string conveniada_sas {get;set;}

		public int codigo_crm_sas {get;set;}

		public string segmentos_sas {get;set;}

		public int alunos_sas {get;set;}

		public string categoria_priv {get;set;}

		public string municipio {get;set;}

		public string sigla_da_uf {get;set;}

		public int populacao_municipio {get;set;}

		public string regiao {get;set;}

		public string cluster {get;set;}

		public string renda_per_capita_municipal {get;set;}

		public string nivel_socioeconomico {get;set;}

		public string rua_numero {get;set;}

		public string bairro {get;set;}

		public string complemento {get;set;}

		public string cep {get;set;}

		public string telefone {get;set;}

		public string media_enem {get;set;}

		public string nota_maior_540 {get;set;}

		public string colocacao_nacional {get;set;}

		public string classificacao_estadual {get;set;}

		public string classificacao_municipal {get;set;}

		public string categoria_enem {get;set;}

		public string inf_ii_iii {get;set;}

		public string inf_iv_v {get;set;}

		public string inf_unificado {get;set;}

		public string infantil {get;set;}

		public string ano_1o {get;set;}

		public string ano_2o {get;set;}

		public string ano_3o {get;set;}

		public string ano_4o {get;set;}

		public string ano_5o {get;set;}

		public string fun_i {get;set;}

		public string ano_6o {get;set;}

		public string ano_7o {get;set;}

		public string ano_8o {get;set;}

		public string ano_9o {get;set;}

		public string fun_ii {get;set;}

		public string serie_1a_em {get;set;}

		public string serie_2a_em {get;set;}

		public string pre_universitario {get;set;}

		public string em {get;set;}

		public string total_alunos {get;set;}

		public string categoria_alunos {get;set;}

		public string abc {get;set;}

		public string a_partir_200_alunos {get;set;}

		public string segmentos {get;set;}

	}

	public class Municipios {
		public string municipio { get; set; }
		public string unidfed { get; set; }
	}


	public class Listas {
		public static List<string> ufsiglas { get; private set; } = new List<string> { "AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MG", "MS", "MT", "PA", "PB", "PE", "PI", "PR", "RJ", "RN", "RR", "RO", "RS", "SC", "SE", "SP", "TO" };
		public static List<string> ufnomes { get; private set; } = new List<string> { "Acre", "Alagoas", "Amazonas", "Amapá", "Bahia", "Ceará", "Distrito Federal", "Espírito Santo", "Goiás", "Maranhão", "Minas Gerais", "Mato Grosso do Sul", "Mato Grosso", "Pará", "Paraíba", "Pernambuco", "Piauí", "Paraná", "Rio de Janeiro", "Rio Grande do Norte", "Roraima", "Rondônia", "Rio Grande do Sul", "Santa Catarina", "Sergipe", "São Paulo", "Tocantins" };
		public static List<string> pracas { get; private set; } = new List<string>() {"Nacional", "Estadual", "Municipal"};
		public static string dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "rankingDB" + ".db3");
	}

}
