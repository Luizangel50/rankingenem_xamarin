﻿using System;
using System.Collections.Generic;

using Foundation;

using UIKit;

namespace RankingENEM
{
	//classe responsável pela inserção das células (UITableViewCells) na UITableView
	//da tela de resultados da busca de municipios
	public class MunicipioTableSource : UITableViewSource
	{
		private List<Municipios> _municipios;
		private string CellIdentifier = "MunicipioTableCell";
		public SelecioneMunicipioViewController _controller;

		public MunicipioTableSource (List<Municipios> municipios, SelecioneMunicipioViewController controller)
		{
			this._municipios = municipios;
			this._controller = controller;
		}

		public override nint RowsInSection(UITableView tableView, nint section) 
		{
			return _municipios.Count;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (CellIdentifier);
			var item = _municipios [indexPath.Row];

			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Value1, CellIdentifier);
			}

			cell.TextLabel.Text = item.municipio;
			cell.TextLabel.AdjustsFontSizeToFitWidth = true;
			cell.DetailTextLabel.Text = item.unidfed;

			cell.DetailTextLabel.TextColor = UIColor.Blue;
			cell.DetailTextLabel.AdjustsLetterSpacingToFitWidth = true;
			cell.DetailTextLabel.AdjustsFontSizeToFitWidth = true;
			cell.TextLabel.AdjustsLetterSpacingToFitWidth = true;
			cell.TextLabel.AdjustsFontSizeToFitWidth = true;
			return cell;
		}

		public override void RowSelected (UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			_controller.municipioSelected = _municipios [indexPath.Row];
			if (_controller.buttonIdentifier.Equals ("DadosMercado")) {
				_controller.PerformSegue ("selecioneMunicipioToDadosPracaSegue", _controller);
			} 
			else if (_controller.buttonIdentifier.Equals ("Rankings")) {
				_controller.PerformSegue ("selecioneMunicipioToRankingsDadosSegue", _controller);
			}

		}
	}
}