﻿using System;
using System.Windows;
using System.Threading;
using System.Collections.Generic;
using System.IO;

//using Xamarin.Forms;

using SQLite;

using Mono.Data.Sqlite;

//[assembly: Dependency(typeof(BdAccess))]

namespace RankingENEM
{
	//classe responsável pela leitura dos dados dos arquivos .csv
	public partial class BdAccess
	{
		public BdAccess ()
		{
		}

		//método que acessa o banco de dados das escolas de cada ano
		//e cria tal banco de dados se for inexistente
		public void access(List<string> insertions)
		{
			this.connection_data = new SQLiteConnection(Listas.dbPath);
			SqliteConnection conn_mono = new SqliteConnection("Data Source=" + Listas.dbPath + ";Version=3;");

			connection_data.DropTable<BaseGeral>();
			connection_data.CreateTable<BaseGeral>();

			if (connection_data.Table<BaseGeral>().Count() == 0)
			{
				SqliteCommand cmd = new SqliteCommand(conn_mono);
				conn_mono.Open();
				var transaction = conn_mono.BeginTransaction();

				cmd.Transaction = transaction;

				foreach (var insert in insertions)
				{
					Console.WriteLine (insert);
					cmd.CommandText = insert;
					cmd.ExecuteNonQuery();
				}

				transaction.Commit();

				cmd.Dispose();
			}
			Console.WriteLine("Reading data");

			//Printando a escolas 
//			var table = this.connection_data.Table<BaseGeral>();
//			foreach (var s in table)
//			{
//				Console.WriteLine(s.CODIGO_DA_ENTIDADE + " " + s.NOME_DA_ENTIDADE + " " + s.SIGLA_DA_UF + " " + s.ANO);
//			}
			conn_mono.Close ();
		}

		//método que adiciona a lista de anos e de unidades federais no banco de dados
		public void buildTables()
		{
			Console.WriteLine("Creating database or opening it");

			this.connection = new SQLiteConnection(Path.Combine(personalPath, "contents.db3"));

//			//*****************************Criando tabela Ano********************************
//			//connection.DropTable<Ano>();
//			this.connection.CreateTable<Ano>();
//
//			try
//			{
//				foreach (int year in years)
//				{
//					this.connection.Execute("INSERT INTO [ano] (ANO_NUMERO) values (?)", year);
//				}
//
//			}
//			catch
//			{
//				Console.WriteLine("Anos nao adicionados corretamente ou já existentes");
//			}
//
//			//Printando a lista de anos
//			//			var tableAnos = connection.Table<Ano>();
//			//			foreach (var s in tableAnos)
//			//			{
//			//			    Console.WriteLine(s.ANO + " " + s.ANO_NUMERO);
//			//			}

			//*****************************Criando tabela UF********************************
			//connection.DropTable<UF>();
			this.connection.CreateTable<UF>();

			try
			{
				for (int i = 0; i < Listas.ufsiglas.Count; i++)
				{
					this.connection.Execute("INSERT INTO [UF] (UNIDF_SIGLA, UNIDF_NOME) values (?,?)", Listas.ufsiglas[i], Listas.ufnomes[i]);
				}
			}
			catch
			{
				Console.WriteLine("UFs nao adicionados corretamente ou já existentes");
			}

			//Printando a lista de UFs
//			var tableUFs = connection.Table<UF>();
//			foreach (var s in tableUFs)
//			{
//			    Console.WriteLine(s.UNIDF_SIGLA + " " + s.UNIDF_NOME);
//			}
		}           

		public SQLiteConnection connection { get; private set; }
		private SQLiteConnection connection_data;

		private string personalPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
	}
}