using System;
using System.Reflection;
using System.CodeDom.Compiler;
using System.Collections.Generic;

using UIKit;

using Foundation;

using CoreGraphics;

namespace RankingENEM
{
	partial class RankingBuscaViewController : UIViewController
	{
		public RankingBuscaViewController (IntPtr handle) : base (handle)
		{
		}

		private string selectedPraca = Listas.pracas[0];
		private string selectedEstado = Listas.ufnomes[0];
		private string editedMunicipio = "";
		private pracaPickerViewModel pracaModel = new pracaPickerViewModel ();
		private estadoOuMunicipioPickerViewModel estadoOuMunicipioModel = new estadoOuMunicipioPickerViewModel ();

		private KeyboardNotifications pracaKeyboard;

		public BdAccess bdaccess;

		private string buttonIdentifier;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			//botão de volta para a tela anterior
			NavigationItem.BackBarButtonItem  = new UIBarButtonItem ("Busca", UIBarButtonItemStyle.Plain, null);
			NavigationItem.SetHidesBackButton (false, true);
			NavigationItem.HidesBackButton = true;

			pracaTextField.RightViewMode = UITextFieldViewMode.Always;
			pracaTextField.RightView = new UIImageView(UIImage.FromFile("arrowdown.gif"));

			SetupPicker ();

			var scrollBuscaPage = new ScrollView ();
			scrollBuscaPage.Frame = new CoreGraphics.CGRect (0, 0, View.Frame.Width, View.Frame.Height);
			scrollBuscaPage.ContentSize = new CGSize (View.Frame.Width, View.Frame.Height);

			View.AddSubview (scrollBuscaPage);
			//adicionar o imageview primeiro
			scrollBuscaPage.AddSubview (rankingBuscaImageView);
			scrollBuscaPage.AddSubview (selecionePracaLabel);
			scrollBuscaPage.AddSubview (pracaTextField);
			scrollBuscaPage.AddSubview (estadoOuMunicipioTextField);
			scrollBuscaPage.AddSubview (dadosMercadoButton);
			scrollBuscaPage.AddSubview (rankingsButton);
			scrollBuscaPage.AddSubview (limparButton);

			pracaKeyboard = new KeyboardNotifications(View, View.Subviews[2] as ScrollView);
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.DidShowNotification, pracaKeyboard.KeyBoardUpNotification);
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, pracaKeyboard.KeyBoardDownNotification);

			//selecionePracaLabel.Text = "Selecione a Praça";

			estadoOuMunicipioTextField.Hidden = true;

			bdaccess = new BdAccess ();
			readInsertions ();

			limparButton.TouchUpInside += delegate {
				
				editedMunicipio = "";

				pracaTextField.Text = "";
				estadoOuMunicipioTextField.Text = "";
				estadoOuMunicipioTextField.Hidden = true;

				//********************************Botao Limpar*********************************
				limparButton.Enabled = false;
				limparButton.SetImage(UIImage.FromFile("trash.gif"), UIControlState.Normal);
				limparButton.SetTitleColor(UIColor.FromRGB(170, 170, 170), UIControlState.Normal);

				//********************************Botao Dados Mercado*********************************
				dadosMercadoButton.Enabled = false;
				dadosMercadoButton.SetImage(UIImage.FromFile("dadosMercado_dis.png"), UIControlState.Normal);
				dadosMercadoButton.SetTitleColor(UIColor.FromRGB(170, 170, 170), UIControlState.Normal);

				//********************************Botao Rankings*********************************
				rankingsButton.Enabled = false;
				rankingsButton.SetImage(UIImage.FromFile("dadosRanking_dis.png"), UIControlState.Normal);
				rankingsButton.SetTitleColor(UIColor.FromRGB(170, 170, 170), UIControlState.Normal);

				pracaTextField.ResignFirstResponder();
				estadoOuMunicipioTextField.ResignFirstResponder();
			};

			dadosMercadoButton.TouchUpInside += (sender, e) => {

				buttonIdentifier = "DadosMercado";
				if (selectedPraca.Equals("Municipal")) {
					//Ir para a tela de busca de municípios
					PerformSegue("municipioSegue", this);
				}
				else {
					//Ir para a tela de dados do mercado
					PerformSegue("rankingBuscaToDadosPracaSegue", this);
				}
			};

			rankingsButton.TouchUpInside += (sender, e) => {

				buttonIdentifier = "Rankings";
				if (selectedPraca.Equals("Municipal")) {
					//Ir para a tela de busca de municípios
					PerformSegue("municipioSegue", this);
				}
				else {
					//Ir para a tela de rankings
					PerformSegue("rankingBuscaToRankingsDadosSegue", this);
				}
			};
		}
			
		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			pracaTextField.ResignFirstResponder ();
			estadoOuMunicipioTextField.ResignFirstResponder ();
		}

		private void SetupPicker()
		{
			//************************ Praça *********************************
			pracaTextField.TextAlignment = UITextAlignment.Center;

			UIPickerView pracaPicker = new UIPickerView();
			pracaPicker.ShowSelectionIndicator = true;
			pracaPicker.Model = pracaModel;

			UIPickerView estadoOuMunicipioPicker = new UIPickerView();
			UIToolbar estadoOuMunicipioToolbar = new UIToolbar();

			pracaModel.PickerChanged += (sender, e) => {
				this.selectedPraca = e.SelectedValue;
				pracaTextField.Text = selectedPraca;

				if (selectedPraca.Equals("Nacional")) {
					estadoOuMunicipioTextField.Hidden = true;
					this.estadoOuMunicipioTextField.InputView = null;
					this.estadoOuMunicipioTextField.InputAccessoryView = null;
				}
				else if (selectedPraca.Equals("Estadual")) {
					estadoOuMunicipioTextField.Hidden = false;
					estadoOuMunicipioTextField.RightViewMode = UITextFieldViewMode.Always;
					estadoOuMunicipioTextField.RightView = new UIImageView(UIImage.FromFile("arrowdown.gif"));
					// Tell the textbox to use the picker for input
					this.estadoOuMunicipioTextField.InputView = estadoOuMunicipioPicker;

					// Display the toolbar over the pickers
					this.estadoOuMunicipioTextField.InputAccessoryView = estadoOuMunicipioToolbar;
					estadoOuMunicipioTextField.Text = selectedEstado;
				}
				else if (selectedPraca.Equals("Municipal")) {
					estadoOuMunicipioTextField.Hidden = false;
					estadoOuMunicipioTextField.RightViewMode = UITextFieldViewMode.Never;
					this.estadoOuMunicipioTextField.InputView = null;
					this.estadoOuMunicipioTextField.InputAccessoryView = null;
					estadoOuMunicipioTextField.Text = editedMunicipio;
				}
			};

			// Setup the toolbar
			UIToolbar pracaToolbar = new UIToolbar();
			pracaToolbar.BarStyle = UIBarStyle.Default;
			//toolbar.Translucent = true;
			pracaToolbar.SizeToFit();

			//Flexible Button
			UIBarButtonItem flexibleButton = new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace);

			// Create a 'done' button for the toolbar and add it to the toolbar
			UIBarButtonItem doneButton = new UIBarButtonItem(UIBarButtonSystemItem.Done,
				(s, e) => {
					this.pracaTextField.Text = selectedPraca.ToString();
					this.pracaTextField.ResignFirstResponder();
				});
			pracaToolbar.SetItems(new UIBarButtonItem[]{flexibleButton, doneButton}, true);

			// Tell the textbox to use the picker for input
			this.pracaTextField.InputView = pracaPicker;

			// Display the toolbar over the pickers
			this.pracaTextField.InputAccessoryView = pracaToolbar;


			//****************************** EstadoOuMunicipio ************************************
			estadoOuMunicipioTextField.TextAlignment = UITextAlignment.Center;

			estadoOuMunicipioPicker.ShowSelectionIndicator = true;
			estadoOuMunicipioPicker.Model = estadoOuMunicipioModel;

			estadoOuMunicipioTextField.EditingDidBegin += (sender, e) =>  {
				if(estadoOuMunicipioTextField.Text.Equals("") && selectedPraca.Equals("Estadual")) {
					estadoOuMunicipioTextField.Text = selectedEstado;
				}
				else if(estadoOuMunicipioTextField.Text.Equals("") && selectedPraca.Equals("Municipal")) {
					estadoOuMunicipioTextField.Text = editedMunicipio;
				}
			};

			estadoOuMunicipioTextField.EditingDidEnd += delegate {				
				estadoOuMunicipioTextField.TextAlignment = UITextAlignment.Center;
			};

			estadoOuMunicipioModel.PickerChanged += (sender, e) => {
				this.selectedEstado = e.SelectedValue;
				estadoOuMunicipioTextField.Text = selectedEstado;
			};

			// Setup the toolbar
			estadoOuMunicipioToolbar.BarStyle = UIBarStyle.Default;
			//toolbar.Translucent = true;
			estadoOuMunicipioToolbar.SizeToFit();

			// Create a 'done' button for the toolbar and add it to the toolbar
			UIBarButtonItem estadoDoneButton = new UIBarButtonItem(UIBarButtonSystemItem.Done,
				(s, e) => {
					this.estadoOuMunicipioTextField.Text = selectedEstado;
					this.estadoOuMunicipioTextField.ResignFirstResponder();
				});
			estadoOuMunicipioToolbar.SetItems(new UIBarButtonItem[]{flexibleButton, estadoDoneButton}, true);

			pracaTextField.EditingDidEnd += delegate {
				pracaTextField.TextAlignment = UITextAlignment.Center;

			};


			//***********************************Praca Text Field***********************************
			pracaTextField.EditingDidBegin += delegate {
				if(pracaTextField.Text.Equals("")) {
					pracaTextField.Text = selectedPraca;

					//********************************Botao Limpar*********************************
					limparButton.Enabled = true;
					limparButton.SetImage(UIImage.FromFile("trash.png"), UIControlState.Normal);
					limparButton.SetTitleColor(UIColor.FromRGB(243, 146, 0), UIControlState.Normal);

					//********************************Botao Dados Mercado*********************************
					dadosMercadoButton.Enabled = true;
					dadosMercadoButton.SetImage(UIImage.FromFile("dadosMercado.png"), UIControlState.Normal);
					dadosMercadoButton.SetTitleColor(UIColor.FromRGB(255, 255, 255), UIControlState.Normal);

					//********************************Botao Rankings*********************************
					rankingsButton.Enabled = true;
					rankingsButton.SetImage(UIImage.FromFile("dadosRanking.png"), UIControlState.Normal);
					rankingsButton.SetTitleColor(UIColor.FromRGB(255, 255, 255), UIControlState.Normal);
				}
				else {
					if (selectedPraca.Equals("Municipal")) {
						editedMunicipio = estadoOuMunicipioTextField.Text;
					}
				}

				if (selectedPraca.Equals("Nacional")) {
					estadoOuMunicipioTextField.Hidden = true;
					this.estadoOuMunicipioTextField.InputView = null;
					this.estadoOuMunicipioTextField.InputAccessoryView = null;
				}
				else if (selectedPraca.Equals("Estadual")) {
					estadoOuMunicipioTextField.Hidden = false;
					estadoOuMunicipioTextField.RightViewMode = UITextFieldViewMode.Always;
					estadoOuMunicipioTextField.RightView = new UIImageView(UIImage.FromFile("arrowdown.gif"));
					// Tell the textbox to use the picker for input
					this.estadoOuMunicipioTextField.InputView = estadoOuMunicipioPicker;

					// Display the toolbar over the pickers
					this.estadoOuMunicipioTextField.InputAccessoryView = estadoOuMunicipioToolbar;
					estadoOuMunicipioTextField.Text = selectedEstado;
				}
				else if (selectedPraca.Equals("Municipal")) {
					estadoOuMunicipioTextField.Hidden = false;
					estadoOuMunicipioTextField.RightViewMode = UITextFieldViewMode.Never;
					this.estadoOuMunicipioTextField.InputView = null;
					this.estadoOuMunicipioTextField.InputAccessoryView = null;
					estadoOuMunicipioTextField.Text = editedMunicipio;
				}
			};
		}

		//Ler as linhas dos arquivos BaseDadosEnem para inserir
		//os dados no banco de dados
		private void readInsertions()
		{
			bdaccess.buildTables();
			List<string> insertions = new List<string>();
			string line;

			string arquivo;
			var assembly = typeof(AppDelegate).GetTypeInfo().Assembly;

			arquivo = "RankingENEM.Resources.BaseInfoEscolas.csv";
			System.IO.Stream stream = assembly.GetManifestResourceStream(arquivo);

			using (System.IO.StreamReader sr = new System.IO.StreamReader(stream))
			{
				while ((line = sr.ReadLine()) != null)
				{
					insertions.Add("insert into [geral] values (" + line + ");");
				}
			}
			bdaccess.access(insertions);
			insertions.Clear();         
		}

		public override void PrepareForSegue (UIStoryboardSegue segue, NSObject sender)
		{
			base.PrepareForSegue (segue, sender);

			if (segue.Identifier.Equals ("rankingBuscaToDadosPracaSegue")) {
				var dadosMercadoViewController = segue.DestinationViewController as DadosMercadoViewController;

				if (dadosMercadoViewController != null) {
					dadosMercadoViewController.pracaSelected = selectedPraca;
					dadosMercadoViewController.estadoOuMunicipioSelected = estadoOuMunicipioTextField.Text;
				}
			} 
			else if (segue.Identifier.Equals ("rankingBuscaToRankingsDadosSegue")) {
				var rankingsDadosViewControlller = segue.DestinationViewController as RankingsDadosViewController;

				if (rankingsDadosViewControlller != null) {					
					rankingsDadosViewControlller.pracaSelected = selectedPraca;
					rankingsDadosViewControlller.estadoOuMunicipioSelected = estadoOuMunicipioTextField.Text;
				}
			}
			else if (segue.Identifier.Equals ("municipioSegue")) {
				var selecioneMunicipioViewController = segue.DestinationViewController as SelecioneMunicipioViewController;

				if (selecioneMunicipioViewController != null) {
					selecioneMunicipioViewController.buttonIdentifier = buttonIdentifier;
					selecioneMunicipioViewController.municipioSearched = estadoOuMunicipioTextField.Text;
				}
			}


		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}			
	}
}
