using Foundation;
using System;
using System.Globalization;
using System.CodeDom.Compiler;
using UIKit;

namespace RankingENEM
{
	partial class InformacoesEscolaViewController : UIViewController
	{
		public InformacoesEscolaViewController (IntPtr handle) : base (handle)
		{
		}

		public DadosBusca escolaSelected;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			CultureInfo brasil = new CultureInfo ("pt-BR");

			codigoINEPLabel.Text += escolaSelected.codigo_inep;
			codigoCRMLabel.Text += escolaSelected.codigo_crm_sas.ToString();
			nomeEscolaLabel.Text += escolaSelected.nome_escola;

			int total_alunos;
			if (int.TryParse (escolaSelected.total_alunos, out total_alunos))
				totalAlunosLabel.Text += total_alunos.ToString ("N0", brasil.NumberFormat);
			else
				totalAlunosLabel.Text += escolaSelected.total_alunos;
			
			double media_enem;
			if (Double.TryParse (escolaSelected.media_enem, out media_enem))
				notaENEMLabel.Text += double.Parse(escolaSelected.media_enem, System.Globalization.CultureInfo.InvariantCulture).ToString("N2", brasil.NumberFormat);
			else
				notaENEMLabel.Text += escolaSelected.media_enem;
			
			conveniadaSASLabel.Text += escolaSelected.conveniada_sas;
			segmentosLabel.Text += escolaSelected.segmentos;
			segmentosSASLabel.Text += escolaSelected.segmentos_sas;
			enderecoLabel.Text += 	escolaSelected.rua_numero 
									+ ", " + escolaSelected.bairro 
									+ ", " + escolaSelected.complemento
									+ ", " + escolaSelected.municipio
									+ " - " + escolaSelected.sigla_da_uf;
			telefoneLabel.Text += escolaSelected.telefone;
		}
	}
}
