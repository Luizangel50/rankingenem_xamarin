// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RankingENEM
{
	[Register ("DadosMercadoViewController")]
	partial class DadosMercadoViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel alunosSASLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIImageView dadosMercadoImageView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel dadosPracaLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel escolasLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel escolasSASLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel escolasTargetLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView escolasTargetTableView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel mediaAlunosEscolaLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel mediaENEMPracaLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel populacaoLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton rankingsButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		NSLayoutConstraint rankingsButtonToBottomConstraint { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		NSLayoutConstraint rankingsButtonToLabelsConstraint { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		NSLayoutConstraint rankingsButtonToTableViewConstraint { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel rendaPerCapitaLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		NSLayoutConstraint rendaPerCapitaToMediaAlunosLabelsConstraint { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		NSLayoutConstraint rendaPerCapitaToTableViewConstraint { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel shareAlunosLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel shareEscolasLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel totalAlunosLabel { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (alunosSASLabel != null) {
				alunosSASLabel.Dispose ();
				alunosSASLabel = null;
			}
			if (dadosMercadoImageView != null) {
				dadosMercadoImageView.Dispose ();
				dadosMercadoImageView = null;
			}
			if (dadosPracaLabel != null) {
				dadosPracaLabel.Dispose ();
				dadosPracaLabel = null;
			}
			if (escolasLabel != null) {
				escolasLabel.Dispose ();
				escolasLabel = null;
			}
			if (escolasSASLabel != null) {
				escolasSASLabel.Dispose ();
				escolasSASLabel = null;
			}
			if (escolasTargetLabel != null) {
				escolasTargetLabel.Dispose ();
				escolasTargetLabel = null;
			}
			if (escolasTargetTableView != null) {
				escolasTargetTableView.Dispose ();
				escolasTargetTableView = null;
			}
			if (mediaAlunosEscolaLabel != null) {
				mediaAlunosEscolaLabel.Dispose ();
				mediaAlunosEscolaLabel = null;
			}
			if (mediaENEMPracaLabel != null) {
				mediaENEMPracaLabel.Dispose ();
				mediaENEMPracaLabel = null;
			}
			if (populacaoLabel != null) {
				populacaoLabel.Dispose ();
				populacaoLabel = null;
			}
			if (rankingsButton != null) {
				rankingsButton.Dispose ();
				rankingsButton = null;
			}
			if (rankingsButtonToBottomConstraint != null) {
				rankingsButtonToBottomConstraint.Dispose ();
				rankingsButtonToBottomConstraint = null;
			}
			if (rankingsButtonToLabelsConstraint != null) {
				rankingsButtonToLabelsConstraint.Dispose ();
				rankingsButtonToLabelsConstraint = null;
			}
			if (rankingsButtonToTableViewConstraint != null) {
				rankingsButtonToTableViewConstraint.Dispose ();
				rankingsButtonToTableViewConstraint = null;
			}
			if (rendaPerCapitaLabel != null) {
				rendaPerCapitaLabel.Dispose ();
				rendaPerCapitaLabel = null;
			}
			if (rendaPerCapitaToMediaAlunosLabelsConstraint != null) {
				rendaPerCapitaToMediaAlunosLabelsConstraint.Dispose ();
				rendaPerCapitaToMediaAlunosLabelsConstraint = null;
			}
			if (rendaPerCapitaToTableViewConstraint != null) {
				rendaPerCapitaToTableViewConstraint.Dispose ();
				rendaPerCapitaToTableViewConstraint = null;
			}
			if (shareAlunosLabel != null) {
				shareAlunosLabel.Dispose ();
				shareAlunosLabel = null;
			}
			if (shareEscolasLabel != null) {
				shareEscolasLabel.Dispose ();
				shareEscolasLabel = null;
			}
			if (totalAlunosLabel != null) {
				totalAlunosLabel.Dispose ();
				totalAlunosLabel = null;
			}
		}
	}
}
